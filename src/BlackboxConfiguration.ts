export interface AgentConfiguration {
    name: string
    port: number
    tls?: {
        key:  string
        cert: string
        ca?: string
    }
}

export interface DriverConfiguration {
    path: string
    onStart?: string
    onStop?: string
}

export interface DeviceConfiguration {
    name: string
    status: string
    type: string
    programmer?: { type: string }
    location?: string
    driver: DriverConfiguration
}

export interface DatabaseConfiguration {
    type: string
    url: string
    dbname: string
}

export interface BlackboxConfiguration {
    agent?: AgentConfiguration
    devices?: [DeviceConfiguration]
    databases?: {[key:string]: DatabaseConfiguration}
}