/* EXAMPLE blackbox-conf.json

{
  "agent": {
    "name":"pi-lab-001",
    "port":8103,
    "tls": {
      "key": <path>,
      "cert": <path>,
      "ca": <path, optional>
    }
  },
  "devices": [
        {
            "name": "pi-lab-001-basys3",
            "status": "stopped",
            "type": "basys3",
            "programmer": {
                "type": "xvc"
            },
            "location": "local",
            "driver": {
                "path": "/opt/blackbox/xvcpi/node/index.js",
                "onStart": "start",
                "onStop": "stop",
                "getInfo": "info"
            }
        }
    ],
    "databases": {
        "user": {
            "type": "mongo",
            "url": "mongodb://localhost:27017",
            "dbname": "ellipsis-user"
        }
    }
}

*/

import * as fs from 'fs'
import * as path from 'path'
import { BlackboxConfiguration } from './BlackboxConfiguration'

export const DEFAULT_PATH = process.env.HOME+'/.blackbox'
export const CONFIG_NAME = 'blackbox-conf.json'
export const CONFIG_PATHS = [DEFAULT_PATH+'/'+CONFIG_NAME, '/etc/'+CONFIG_NAME]
export const DEFAULT_PORT = 8100
export const ERROR_CONFIG_NOT_FOUND = 'Blackbox config not found.'

// Read or setup config:
export function loadConfig(): BlackboxConfiguration {
    let config: any = undefined
    for(let strPath of CONFIG_PATHS) {
        const p = path.resolve(strPath)
        if(fs.existsSync(p)) {
            config = JSON.parse(fs.readFileSync(p).toString())
            if(config) {
                if(!config.agent) {
                    throw new Error(`Blackbox config found at path '${strPath}' but agent element not found.`)
                }
                
                // Replace name with command line argument if present:
                if(process.argv[2]) {
                    config.agent.name = process.argv[2]
                } else if(!config.agent.name) {
                    throw new Error(`Blackbox config found at path '${strPath}' but agent name not found.`)
                }

                // Replace port with command line argument if present:
                if(process.argv[3]) {
                    config.agent.port = parseInt(process.argv[3])
                } else if(!config.agent.port) {
                    config.agent.port = DEFAULT_PORT
                }

                return config
            } else {
                throw new Error(`Failed to load config from '${strPath}.`)
            }
        }
    }
    throw new Error(`Config file not found.`)
}