import { BlackboxConfiguration } from './BlackboxConfiguration';
export declare const DEFAULT_PATH: string;
export declare const CONFIG_NAME = "blackbox-conf.json";
export declare const CONFIG_PATHS: string[];
export declare const DEFAULT_PORT = 8100;
export declare const ERROR_CONFIG_NOT_FOUND = "Blackbox config not found.";
export declare function loadConfig(): BlackboxConfiguration;
