"use strict";
/* EXAMPLE blackbox-conf.json

{
  "agent": {
    "name":"pi-lab-001",
    "port":8103,
    "tls": {
      "key": <path>,
      "cert": <path>,
      "ca": <path, optional>
    }
  },
  "devices": [
        {
            "name": "pi-lab-001-basys3",
            "status": "stopped",
            "type": "basys3",
            "programmer": {
                "type": "xvc"
            },
            "location": "local",
            "driver": {
                "path": "/opt/blackbox/xvcpi/node/index.js",
                "onStart": "start",
                "onStop": "stop",
                "getInfo": "info"
            }
        }
    ],
    "databases": {
        "user": {
            "type": "mongo",
            "url": "mongodb://localhost:27017",
            "dbname": "ellipsis-user"
        }
    }
}

*/
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadConfig = exports.ERROR_CONFIG_NOT_FOUND = exports.DEFAULT_PORT = exports.CONFIG_PATHS = exports.CONFIG_NAME = exports.DEFAULT_PATH = void 0;
var fs = require("fs");
var path = require("path");
exports.DEFAULT_PATH = process.env.HOME + '/.blackbox';
exports.CONFIG_NAME = 'blackbox-conf.json';
exports.CONFIG_PATHS = [exports.DEFAULT_PATH + '/' + exports.CONFIG_NAME, '/etc/' + exports.CONFIG_NAME];
exports.DEFAULT_PORT = 8100;
exports.ERROR_CONFIG_NOT_FOUND = 'Blackbox config not found.';
// Read or setup config:
function loadConfig() {
    var config = undefined;
    for (var _i = 0, CONFIG_PATHS_1 = exports.CONFIG_PATHS; _i < CONFIG_PATHS_1.length; _i++) {
        var strPath = CONFIG_PATHS_1[_i];
        var p = path.resolve(strPath);
        if (fs.existsSync(p)) {
            config = JSON.parse(fs.readFileSync(p).toString());
            if (config) {
                if (!config.agent) {
                    throw new Error("Blackbox config found at path '" + strPath + "' but agent element not found.");
                }
                // Replace name with command line argument if present:
                if (process.argv[2]) {
                    config.agent.name = process.argv[2];
                }
                else if (!config.agent.name) {
                    throw new Error("Blackbox config found at path '" + strPath + "' but agent name not found.");
                }
                // Replace port with command line argument if present:
                if (process.argv[3]) {
                    config.agent.port = parseInt(process.argv[3]);
                }
                else if (!config.agent.port) {
                    config.agent.port = exports.DEFAULT_PORT;
                }
                return config;
            }
            else {
                throw new Error("Failed to load config from '" + strPath + ".");
            }
        }
    }
    throw new Error("Config file not found.");
}
exports.loadConfig = loadConfig;
